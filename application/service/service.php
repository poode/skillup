<?php

/**
 *
 */
class Service
{

  function __construct()
  {
    $array = [];
    $path ='application/model';
    $dir = array_diff(scandir($path), array('..', '.'));

    foreach ($dir as $key => $file) {
      if ($file !== 'index.php') {
        require_once $path.'/'.$file;
        $array[$key]= ucfirst(rtrim($file,".php"));
        $Object[$key] = new $array[$key];
      }
    }
    // echo "<pre>";print_r($Object[3]->Orm_obj->view);die;
  }
  static function ModelClass($model='') {
    $dir = scandir('application/model');
    // var_dump($dir);
    $ViPhp = $model.'.php';
    foreach ($dir as $key => $fileOrFolder) {
      if ($fileOrFolder === $ViPhp) {
        require_once 'application/model/'.$model.'.php';
        $tokens = token_get_all(file_get_contents('application/model/'.$model.'.php')); //this to check inside php file if has class named with controller or not to avoid making object of non class file
        foreach ($tokens as $token) {
          $countToken = count($token);
          for ($i= 0; $i < $countToken ; $i++) {
            if ($token[$i] === 'class') {
              $classFound = $token[$i];
              $Model = ucfirst($model);
              $calssObj = new $Model();
              // echo "<pre>";print_r(get_class_methods($calssObj)); die;
              return $calssObj;
            }
          }
        }
      }
    }
  }
}
