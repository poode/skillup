<?php
/**
 *
 */
require_once 'core/main_model.php';
require_once 'application/service/service.php';

class Main_Controller
{

  function __construct($argument='')
  {
    $this->load_model = new Main_Model();
    echo "this main controller <br>";
    return $this->load_model;
   }

   public function render($view='',$data='') {
     if (strpos($view, '/') !== false) {
       $file =  substr($view, strrpos($view, '/') + 1);
       $mainDir = rtrim($view, $file);                    // get directory without last word after last slash
       $dir = scandir($mainDir);
       $ViPhp = $file.'.php';
//           echo "<pre>"; var_dump($mainDir); var_dump($dir); die;
       foreach ($dir as $key => $fileOrFolder) {
         if ($fileOrFolder === $ViPhp) {
           include($view.'.php');
           return $data;
         }
       }
     }
     else {
       $dir = scandir('application/views');
       $ViPhp = $view.'.php';
       foreach ($dir as $key => $fileOrFolder) {
         if ($fileOrFolder === $ViPhp) {
           include('application/views/'.$view.'.php');
           return $data;
         }
       }
     }
   }

    static function form_open ($action='',$HtmlClass ='') {  // $action should be the same url you opened the form from it or to another custom page
        if ($HtmlClass !== '') {
            $string = '<form action="'.$action.'" calss = "'.$HtmlClass.'" method= "post" name= "formData" accept-charset="utf-8" autocomplete="on">';
            echo  $string;
            return false;
        }
        $HtmlClass = $action;
        $string = '<form action="service/service.php" calss = "'.$HtmlClass.'" method= "post" name= "formData" accept-charset="utf-8" autocomplete="on">';
        echo  $string;
    }

    static function Load_model($model='')
    {
      return Service::ModelClass($model);
    }
}
