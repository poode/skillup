<?php

require_once 'core/main_controller.php';
require_once 'core/orm.php';
require_once 'core/main_model.php';


if (isset($_GET['url'])) {
  $NoElem = count($_GET['url']);
  $classFound = false;
  $classNameFound = false;
  $url = rtrim($_GET['url'],'/');
  $urlParts = explode('/',$url);
  if (isset($urlParts[0])) {
    $controller = $urlParts[0];
  }
  if (isset($urlParts[1])) {
    $method = $urlParts[1];
  }
  if (isset($urlParts[2])) {
    $argument = $urlParts[2];
  }

  if(file_exists ('application/controller/'.$controller.'.php')) {
        require_once 'application/controller/'.$controller.'.php';
        echo "controller is $controller this message from autoload.php<br>";
          $tokens = token_get_all(file_get_contents('application/controller/'.$controller.'.php')); //this to check inside php file if has class named with controller or not to avoid making object of non class file
          foreach ($tokens as $token) {
            $countToken = count($token);
            for ($i= 0; $i < $countToken ; $i++) {
              if ($token[$i] === 'class') {
                $classFound = $token[$i];
              }
              if ($token[$i] === ucfirst($controller)) {
                $classNameFound = $token[$i];
              }
            }
          }
        if (!$classFound && $classNameFound !== $controller) {
          echo "<h1>Called Controller has no class inside<h1>";
          echo file_get_contents ('application/views/error/404.php');
          return false;
        }
        $controller_obj = new $controller();
      if (isset($method)) {
          if (method_exists($controller,$method)) {
            echo "this is method $method from controller $controller and this message from autoload.php<br>";
              if (isset($argument)) {
                echo $controller_obj->$method($argument)."<br>";
              }
              else {
                require_once 'application/controller/'.$controller.'.php';
                $ctrl = ucfirst($controller);
                $ctrl_Obj = new $ctrl;
                // echo "<pre>"; var_dump($ctrl_Obj->loading()); die;
                echo $ctrl_Obj->$method();
              }
          }
          else {
            echo file_get_contents ('application/views/error/404.php');
            return false;
          }
     }
     else if ($NoElem === 1 && method_exists($controller,'index')) {
       $controller_obj->index();
     }
  }
  else {
        echo file_get_contents ('application/views/error/404.php');
        return false;
  }
}
else {
  // here we will add index page as a required logic require_once

  $Object = new Main_Model();
  // $view = $Object->Orm_obj->view;
  // echo "<pre>";var_dump(file_exists ('application/views/'.$view.'.php')); die;

    if (isset($Object->Orm_obj->view)) {
      $view = $Object->Orm_obj->view;
      if(file_exists ('./application/views/'.$view.'.php')) {
        echo file_get_contents ('application/views/'.$view.'.php');
      }
      else {
        echo file_get_contents ('application/views/error/404.php');
        return false;
      }
      // echo "No controller Found from autoload.php<br>";
    }

    // $Orm_Obj = new Main_Model();
    // // echo "<pre>";var_dump($Orm_Obj->Orm_obj); die;
    // $m = array('p' => 5);
    // echo $Orm_Obj->Orm_obj->update($m,'table', 'id');

}
